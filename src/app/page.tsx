"use client";

import { useState, FormEvent, ChangeEvent } from "react";
import styles from "./page.module.css";

export default function Home() {
  const [prompt, setPrompt] = useState<string>("");
  const [result, setResult] = useState<string>();

  async function onSubmit(event: FormEvent<HTMLFormElement>): Promise<any> {
    event.preventDefault();
    try {
      const response = await fetch("/api/generate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ prompt: prompt }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw (
          data.error ||
          new Error(`Request failed with status ${response.status}`)
        );
      }

      setResult(data.result);
      setPrompt("");
    } catch (error) {
      console.error(error);
      if (error instanceof Error) {
        alert(error.message);
      }
    }
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setPrompt(event.target.value);
  };

  return (
    <main className={styles.main}>
      <div className={styles.description}>Welcome to Rendezvous Chatbot</div>

      <div>
        <h3>Enter your input here:</h3>
        <form className={styles.form} onSubmit={onSubmit}>
          <input
            type="text"
            placeholder="Please enter your prompt"
            value={prompt}
            onChange={(e) => handleChange(e)}
          />
          <input type="submit" value="Generate Result" />
        </form>
        <div className={styles.result}>{result}</div>
      </div>

      <div className={styles.grid}></div>
    </main>
  );
}
