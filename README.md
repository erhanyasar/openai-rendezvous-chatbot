# Randezvous Chatbot with OpenAI's Natural Language Processing with Next.js

## Given Brief

Build an API endpoint that extracts the user intent for a new appointment from a natural language message in Turkish using GPT4.

- Extract both relative and absolute time and date.
- Handle "bugün", "yarın", "bu Cuma", "30 Temmuz" No "parsing" natural language - GPT4 should be doing that for you.
- Given a message like "Yarin oglen 2'de musait misiniz?" the API endpoint should output JSON similar to the following;
  (this example assumes the date is 2023-07-29): { intent: "new_appointment", datetimeStr: "2023-07-30T14:00" }
- Your submission should include a suite of tests that call the API endpoint with a few different
  example inputs.
- Tests should assert that the correct intent and datetime is extracted, also assert that intents other than new appointments are classified as "other."

## Getting Started

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

To see this project in production, please visit `https://openai-rendezvous-chatbot.vercel.app/`.

### Setup

1. If you don’t have Node.js installed, [install it from here](https://nodejs.org/en/) (Node.js version >= 14.6.0 required)

2. Clone this repository

3. Navigate into the project directory

   ```bash
   $ cd openai-rendezvous-chatbot
   ```

4. Install the requirements

   ```bash
   $ npm install
   ```

5. Make a copy of the example environment variables file

   On Linux systems:

   ```bash
   $ cp .env.example .env
   ```

   On Windows:

   ```powershell
   $ copy .env.example .env
   ```

6. Add your [API key](https://platform.openai.com/account/api-keys) to the newly created `.env` file

7. Run the app

   ```bash
   npm run dev
   # or
   yarn dev
   # or
   pnpm dev
   ```

You should now be able to access the app at [http://localhost:3000](http://localhost:3000).

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
