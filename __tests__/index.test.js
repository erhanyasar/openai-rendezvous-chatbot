import { render, screen } from "@testing-library/react";
import Home from "../src/app/page";
import "@testing-library/jest-dom";

describe("Home", () => {
  it("renders a heading", () => {
    render(<Home />);
    const heading = screen.getByText("Welcome to Rendezvous Chatbot");
    expect(heading).toBeInTheDocument();
  });
  it("renders form title", () => {
    render(<Home />);
    const heading = screen.getByText("Enter your input here:");
    expect(heading).toBeInTheDocument();
  });
  it("renders form input placeholder", () => {
    render(<Home />);
    const heading = screen.getByPlaceholderText("Please enter your prompt");
    expect(heading).toBeInTheDocument();
  });
  it("renders form button", () => {
    render(<Home />);
    const heading = screen.getByText("Generate Result");
    expect(heading).toBeInTheDocument();
  });
});
