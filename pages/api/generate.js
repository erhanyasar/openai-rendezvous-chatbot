import { Configuration, OpenAIApi } from "openai";

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
  if (!configuration.apiKey) {
    res.status(500).json({
      error: {
        message: "OpenAI API key not configured.",
      },
    });
    return;
  }

  const prompt = req.body.prompt || "";
  if (prompt.trim().length === 0) {
    res.status(400).json({
      error: {
        message: "Please enter a valid input.",
      },
    });
    return;
  }

  try {
    /*
    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: generatePrompt(prompt),
      temperature: 0.6,
    });
    res.status(200).json({ result: completion.data.choices[0].text });
    */
    const completion = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: generateMessage(prompt),
      temperature: 0.6,
    });
    res
      .status(200)
      .json({ result: completion.data.choices[0].message.content });
  } catch (error) {
    if (error.response) {
      console.error(error.response.status, error.response.data);
      res.status(error.response.status).json(error.response.data);
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`);
      res.status(500).json({
        error: {
          message: "An error occurred during your request.",
        },
      });
    }
  }
}

function generateMessage(message) {
  const capitalizedPrompt =
    message[0].toUpperCase() + message.slice(1).toLowerCase();

  return [
    {
      role: "system",
      content: `You are a helpful chatbot as an assistant for a commercial product. For given messages below, return a JSON object
      containing intent and dateTimeStr properties. For intent, it's either returning "new appointment" if content contains
      time and date. For dateTimeStr property, return desired date as a Javascript date object. For example
      "{ intent: "new_appointment", dateTimeStr: "2023-07-30T14:00" }". For content like nothing to do with a randezvous,
      mark intent as "other" and only return empty as dateTimeStr property. Consider the date is 2023-07-29 today.`,
    },
    { role: "user", content: "Yarın öğlen 2'de müsait misiniz?" },
    {
      role: "assistant",
      content: '{ intent: "new_appointment", dateTimeStr: "2023-07-30T14:00" }',
    },
    { role: "user", content: "Bugün için saat 4'te müsaitliğiniz var mı?" },
    {
      role: "assistant",
      content: '{ intent: "new_appointment", dateTimeStr: "2023-07-29T16:00" }',
    },
    { role: "user", content: "Bu Cuma 3'te yardımcı olabilir misiniz?" },
    {
      role: "assistant",
      content: '{ intent: "new_appointment", dateTimeStr: "2023-08-04T15:00" }',
    },
    {
      role: "user",
      content: "30 Temmuz'da öğlen 1'de acil ihtiyacım var, müsait misiniz?",
    },
    {
      role: "assistant",
      content: '{ intent: "new_appointment", dateTimeStr: "2023-07-30T18:00" }',
    },
    { role: "user", content: "Nasılsın?" },
    { role: "assistant", content: '{ intent: "other", dateTimeStr: "" }' },
    { role: "user", content: `${capitalizedPrompt}` },
  ];
}

function generatePrompt(prompt) {
  const capitalizedPrompt =
    prompt[0].toUpperCase() + prompt.slice(1).toLowerCase();

  return `
    For given prompts below, return an array consisting a JSON object like [{ intent: "new_appointment", dateTimeStr: "2023-07-30T14:00" }].
    For prompts like nothing to do with a randezvous, mark it's intention as other and return either with a dateTimeStr or none. 
    Considering the date is 2023-07-29 today, the results should be as followed,

    prompt: Yarin öğlen 2'de musait misiniz?
    Result: [{ intent: "new_appointment", dateTimeStr: "2023-07-30T14:00" }]
    prompt: Bugün için saat 4'te müsaitliğiniz var mı?
    Result: [{ intent: "new_appointment", dateTimeStr: "2023-07-29T16:00" }]
    prompt: Bu Cuma 3'te yardımcı olabilir misiniz?
    Result: [{ intent: "new_appointment", dateTimeStr: "2023-08-04T15:00" }]
    prompt: 30 Temmuz'da öğlen 1'de acil ihtiyacım var, müsait misiniz?
    Result: [{ intent: "new_appointment", dateTimeStr: "2023-07-30T18:00" }]
    prompt: Nasılsın?
    Result: [{ intent: "other" }]
    prompt: ${capitalizedPrompt}
    Result:
    `;
}
